# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Python Proton client module "
HOMEPAGE="https://protonvpn.com/"

PYTHON_COMPAT=( python3_{7..11} )

inherit distutils-r1

EGIT_REPO_URI="https://github.com/ProtonMail/${PN}.git"
EGIT_COMMIT="${PV}"

inherit git-r3
KEYWORDS="amd64 ~arm64 x86"

LICENSE="GPL-3"
SLOT="0"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${CDEPEND}
	${PYTHON_DEPS}
	dev-python/requests
	dev-python/bcrypt
	dev-python/python-gnupg
	dev-python/pyopenssl
"

BDEPEND=$RDEPEND
DEPEND=$RDEPEND
