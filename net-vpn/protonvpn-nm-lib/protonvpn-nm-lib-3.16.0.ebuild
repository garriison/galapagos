# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="The ProtonVPN NM (NetworkManager) Library is intended for every ProtonVPN service user."
HOMEPAGE="https://protonvpn.com/"

PYTHON_COMPAT=( python3_{7..12} )

inherit distutils-r1

#SRC_URI="https://github.com/ProtonVPN/${PN}/archive/refs/tags/${PV}.tar.gz"
EGIT_REPO_URI="https://github.com/ProtonVPN/${PN}.git"
EGIT_COMMIT="${PV}"

inherit git-r3
KEYWORDS="amd64 ~arm64 x86"

LICENSE="GPL-3"
SLOT="0"

IUSE="systemd"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

CDEPEND="
	systemd? ( sys-apps/systemd )
"

RDEPEND="${CDEPEND}
	${PYTHON_DEPS}
	acct-group/openvpn
	acct-user/openvpn
	app-crypt/libsecret
	dev-python/distro
	dev-python/jinja
	dev-python/keyring
	dev-python/pygobject
	dev-python/pyxdg
	gnome-base/gnome-keyring
	net-misc/networkmanager
	net-vpn/networkmanager-openvpn
	net-vpn/openvpn
	net-vpn/proton-python-client
	sys-apps/dbus
	sys-auth/polkit
"

BDEPEND=$RDEPEND
DEPEND=$RDEPEND
