# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Linux command-line client for ProtonVPN. Written in Python. "
HOMEPAGE="https://protonvpn.com/"

PYTHON_COMPAT=( python3_{7..12} )

inherit distutils-r1

EGIT_REPO_URI="https://github.com/Rafficer/linux-cli-community.git"
EGIT_COMMIT="v${PV}"

inherit git-r3
KEYWORDS="amd64 ~arm64 x86"

LICENSE="GPL-3"
SLOT="0"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${CDEPEND}
	${PYTHON_DEPS}
	dev-python/pythondialog
	dev-python/dnspython
	dev-python/jinja
	dev-python/requests
	dev-python/docopt
"

BDEPEND=$RDEPEND
DEPEND=$RDEPEND
